//
//  ViewController.swift
//  FireBaseApp
//
//  Created by Mounika Nerella on 8/24/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase

@objc(EmailViewController)
class ViewController: UIViewController {

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var userName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func createAcoount(_ sender: UIButton) {
        if userName.text == ""
        {
            let alertController = UIAlertController(title: "Error", message: "Please enter", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
            present(alertController,animated: true,completion: nil)
        }
        else{
            Auth.auth().createUser(withEmail: userName.text!, password: password.text!) { (user, error) in
                if error == nil{
                    print("You have successfully signed up")
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "Home")
                    self.present(controller!, animated: true, completion: nil)
                } else{
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
       
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

